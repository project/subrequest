Subrequest Drupal module
========================

Description
-------------
The module allows you to make an internal sub-request and display the response
in a block.

Installation
-------------
As usual.

Configuration
-------------
Navigate to admin/structure/block page and place 'subrequest' block to desired
region. In the block configuration form specify an internal url which you want
to request.
